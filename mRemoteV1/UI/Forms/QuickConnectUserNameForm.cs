﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using mRemoteNG.Security;
using mRemoteNG.Tools;

namespace mRemoteNG.UI.Forms
{
    public partial class QuickConnectUserNameForm : Form
    {
        private SecureString _userName = new SecureString();

        public QuickConnectUserNameForm()
        {
            InitializeComponent();
        }

        public Optional<SecureString> GetKey()
        {
            var dialog = ShowDialog();
            return dialog == DialogResult.OK
                ? _userName
                : Optional<SecureString>.Empty;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void QuickConnectUserNameForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _userName = txtUserName.Text.ConvertToSecureString();
            txtUserName.Text = "";
        }
    }
}
